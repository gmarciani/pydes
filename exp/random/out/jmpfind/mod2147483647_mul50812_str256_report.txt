
==========================================================
                          JUMPER                          
==========================================================

                         General                          
Modulus........................................2147483647
Multiplier..........................................50812
Streams...............................................256

                          Result                          
Jumpers...............................................354
Best Jumper.........................................29872
Best Jump Size....................................8362647
